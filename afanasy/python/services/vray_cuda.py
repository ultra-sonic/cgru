# -*- coding: utf-8 -*-
import socket
from services import service

parser = 'vray'

str_hosts = '@AF_HOSTS@'
str_hostsprefix = '-renderhost='
str_hostseparator = ';'


class vray_cuda(service.service):
	"""
	VRay Standalone CUDA
	the following code is redundant - needs to be inherited from vray...
	"""

	# override base service class method
	def __init__(self, taskInfo, i_verbose):
		#super(service.vray,service.service)
		print('vray.init')
		service.service.__init__(self, taskInfo, i_verbose)
		self.str_hosts = str_hosts
		self.str_hostsprefix = str_hostsprefix
		self.str_hostseparator = str_hostseparator




	def applyCmdHosts(self, command):
		"""Missing DocString

		:param command:
		:return:
		"""
		self.str_hosts = str_hosts
		self.str_hostsprefix = str_hostsprefix
		self.str_hostseparator = str_hostseparator

		if len(self.taskInfo['hosts']):
			hosts = self.str_hostsprefix+'"' # due to the ; seperator vray needs additional quotation marks
			firsthost = True
			for host in self.taskInfo['hosts']:
				if firsthost:
					firsthost = False
				else:
					hosts += self.str_hostseparator
				hosts += host
			hosts+='"' # due to the ; seperator vray needs additional quotation marks
			command = command.replace(self.str_hosts, hosts)
			print('VRay hosts list "%s" applied:' % str(hosts))
		else:
			command = command.replace(self.str_hosts, "")
		print(command)
		return command

