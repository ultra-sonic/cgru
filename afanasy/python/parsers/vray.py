from parsers import parser

import re

re_frame = re.compile(
	r'SCEN.*(progr: begin scene preprocessing for frame )([0-9]+)'
)
re_number = re.compile(r'[0-9]+')
re_percent = re.compile(
	r'Rendering image...:([ ]{,})([0-9]{1,2}.*)(%[ ]{,}).*'
)

Warnings = ['Warning']
Errors = ['Error loading irradiance map','obtain a licence','Fatal','Error writing render region','Could not load mesh file', 'V-Ray error:' , 'Cannot create output image file']
ErrorsRE = [re.compile(r'Error loading geometry .* from stdin') , re.compile(r'Directory.*does not exist and could not be created') , re.compile(r'Data decompression.*failed') ]
NoErrors = ['Closing log'] 


class vray(parser.parser):
	"""VRay Standalone
	"""

	def __init__(self):
		parser.parser.__init__(self)
		self.buffer = ""
		self.numinseq = 0

	def do(self, data, mode):
		"""Missing DocString

		:param data:
		:param mode:
		:return:
		"""
		# self.buffer += data
		# needcalc = False
		# frame = False
		for noerror in NoErrors:
			if data.find(noerror) != -1:
				break
		for warning in Warnings:
			if data.find(warning) != -1:
				self.warning = True
				break
		for error in Errors:
			if data.find(error) != -1:
				self.error = True
				break
		for errorRE in ErrorsRE:
			if errorRE.search( data) is not None:
				self.error = True
				break

		if len(data) < 1:
			return

		match = re_percent.findall(data)
		if len(match):
			percentframe = float(match[-1][1])
			self.percent = int(percentframe)
