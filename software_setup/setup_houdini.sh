#!/bin/bash
echo "---------------------------------------------------------------"
echo "----- setup_houdini.sh"
echo "---------------------------------------------------------------"

# Source general for all soft directives:
# source "$CGRU_LOCATION/software_setup/setup__all.sh"

# Search directory where Houdini installed:
HOUDINI_INSTALL_DIR="/opt"
#for folder in `ls "$HOUDINI_INSTALL_DIR"`; do
#	if [ "`echo $folder | awk '{print match( \$1, "hfs")}'`" == "1" ]; then
#		export HOUDINI_LOCATION="${HOUDINI_INSTALL_DIR}/${folder}"
#	fi
#done
export HOUDINI_LOCATION="${HOUDINI_INSTALL_DIR}/hfs${HOUDINI_VERSION}"

#Override houdini location based on locate_houdini.sh
# locate_houdini="$CGRU_LOCATION/software_setup/locate_houdini.sh"
# if [ -f $locate_houdini ]; then
#	source $locate_houdini
#	HOUDINI_LOCATION="$APP_DIR"
# fi

# Check Houdini location:
if [ -z "$HOUDINI_LOCATION" ]; then
	echo "Can't find houdini in '$HOUDINI_INSTALL_DIR'"
	exit 1
fi
echo "Houdini location = '$HOUDINI_LOCATION'"

export HSITE="/NAS/development/production/houdini"

# Source Houdini setup shell script:
pushd $HOUDINI_LOCATION >> /dev/null
source houdini_setup_bash
popd $pwd >> /dev/null

# Houdini Lic Server
# export SESI_LMHOST="mcs"
# hserver -S $SESI_LMHOST

# Setup CGRU houdini scripts location:
export HOUDINI_CGRU_PATH=$CGRU_LOCATION/plugins/houdini

# Set Python path to afanasy submission script:
export PYTHONPATH=$HOUDINI_CGRU_PATH:$PYTHONPATH

# Define OTL scan path:
HOUDINI_CGRU_OTLSCAN_PATH=$HIH/otls:$HOUDINI_CGRU_PATH

# Create or add to exist OTL scan path:
# if [ "$HOUDINI_OTLSCAN_PATH" != "" ]; then
#	export HOUDINI_OTLSCAN_PATH="${HOUDINI_CGRU_OTLSCAN_PATH}:${HOUDINI_OTLSCAN_PATH}"
# else
export HOUDINI_OTLSCAN_PATH=${HOUDINI_OTLSCAN_PATH}:@/otls:${HOUDINI_CGRU_OTLSCAN_PATH}
# fi

# QLIB variable should point to the downloaded and extracted qLib package
# ($HIH is the $HOME/houdiniXX.X folder)
#
QLIB=/localhost/repositories/houdini/qLib
QOTL=$QLIB/otls

export HOUDINI_OTLSCAN_PATH=${HOUDINI_OTLSCAN_PATH}:$HIH/otls/wip:$QOTL/base:$QOTL/future:$QOTL/experimental
export HOUDINI_GALLERY_PATH=$QLIB/gallery:@/gallery
export HOUDINI_TOOLBAR_PATH=$QLIB/toolbar:@/toolbar
export HOUDINI_SCRIPT_PATH=$QLIB/scripts:@/scripts
# end QLIB

export APP_DIR="$HOUDINI_LOCATION"
export APP_EXE="houdini"


